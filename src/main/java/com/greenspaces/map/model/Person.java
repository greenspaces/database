package com.greenspaces.map.model;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import lombok.Getter;
import lombok.Setter;


@Entity
public class Person {
    public Person(){}

    public Person(Double latitude, Double longitude, Integer height, Integer age, Date date) {
        super();
        this.latitude = latitude;
        this.longitude = longitude;
        this.height = height;
        this.age = age;
        this.date = date;
    }
    @Id
    @Getter
    @GeneratedValue(strategy= GenerationType.AUTO)
    private long id;
    @Setter @Getter private Double latitude;
    @Setter @Getter private Double longitude;
    @Setter @Getter private Integer height;
    @Setter @Getter private Integer age;
    @Setter @Getter private Date date;

}
