package com.greenspaces.map.repository;

import com.greenspaces.map.model.Person;
import org.springframework.data.repository.CrudRepository;

public interface PersonRepository extends CrudRepository<Person, Long> {
}
